//
//  TestCurrencyTableViewCell.swift
//  xChangeTests
//
//  Created by Danko, Radoslav on 20/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import XCTest
@testable import xChange

class TestCurrencyTableViewCell: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testReuseIdentifier() {
        XCTAssert(CurrencyTableViewCell.reuseIdentifier == "currencyCell")
    }

}
