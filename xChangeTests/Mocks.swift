//
//  Mocks.swift
//  xChangeTests
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

enum CustomError: String, LocalizedError {
    case invalidData = "Invalid data"
}

struct MockData {
    public static let correctData = """
                        {
                        "GBPUSD": 1.3096,
                        "USDGBP": 0.7881
                        }
                        """.data(using: .utf8)
    public static let incorrectData = """
                        {
                        "invalidObject":["GBPUSD": 1.3096]
                        }
                        """.data(using: .utf8)


}
