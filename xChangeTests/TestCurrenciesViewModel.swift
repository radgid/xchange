//
//  TestCurrenciesViewModel.swift
//  xChangeTests
//
//  Created by Danko, Radoslav on 20/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import XCTest
@testable import xChange

class TestCurrenciesViewModel: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCurrenciesListNotEmpty() {
        let vm = CurrenciesViewModel()
        XCTAssert(vm.currencies.count > 0, "The list of currencies must not be empty")
    }

    func testCurrenciesListCountMatchesNumberOfRows() {
        let vm = CurrenciesViewModel()
        XCTAssert(vm.currencies.count == vm.numberOfRows, "Currencies count must match array count")
    }

    func testCurrenciesNumberOfSectionsIsOne() {
        let vm = CurrenciesViewModel()
        XCTAssert(vm.numberOfSections == 1, "Currencies in one section only")
    }

    func testCurrencyPairIsEmptyByDefault() {
        let vm = CurrenciesViewModel()
        XCTAssert(vm.currencyPair() == "", "Curency Pair is empty by default")
    }

    func testCurrencyValidIndexPathNotNil() {
        let vm = CurrenciesViewModel()
        XCTAssert(vm.indexPath(for: "GBP") != nil, "Valid currency not found")
    }

    func testCurrencyInvalidIndexPathNil() {
        let vm = CurrenciesViewModel()
        XCTAssert(vm.indexPath(for: "AAA") == nil, "Invalid currency found")
    }

    func testValidIndexPathYieldsCurrency() {
        let vm = CurrenciesViewModel()
        XCTAssert(vm.currency(for: IndexPath(row: 1, section: 0)) != nil, "IndexPath should yield currency")
    }

    func testInvalidIndexPathReturnsNil() {
        let vm = CurrenciesViewModel()
        XCTAssert(vm.currency(for: IndexPath(row: 1000, section: 0)) == nil, "IndexPath beyond element count should yield nil")
    }

}
