//
//  xChangeTests.swift
//  xChangeTests
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import XCTest
@testable import xChange

class TestAPI: XCTestCase {

//    fileprivate var urlSession: MockURLSession?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
//        urlSession = MockURLSession()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCurrencyPairsValid() {

        //given
        let expectation = XCTestExpectation(description: "Test Valid Currency Pairs data")
        var currencyEmpty = false
        var apiError: Error?
        let config = URLSessionConfiguration.default
        config.protocolClasses = [MockURLProtocol.self]
        let urlSession = URLSession(configuration: config)
        let api = RestApi(urlSession: urlSession)

        let data = MockData.correctData

        MockURLProtocol.requestHandler = { request in
             guard let url = request.url else {
               throw ApiError.invalidUrl
             }

             let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
             return (response, data)
           }

        //when
        api.getExchangeRates(for: ["USD:EUR"]) { currencyPairs, error in
            expectation.fulfill()
            currencyEmpty = currencyPairs?.isEmpty ?? true
            apiError = error
        }

        wait(for: [expectation], timeout: 3)

        //then
        XCTAssertFalse(currencyEmpty, "List of currency pairs is empty")
        XCTAssertNil(apiError)
    }

    func testCurrencyPairsInvalid() {

        //given
        let expectation = XCTestExpectation(description: "Test Invalid Currency Pairs data")
        var currencyEmpty = false
        var apiError: Error?
        let config = URLSessionConfiguration.default
        config.protocolClasses = [MockURLProtocol.self]
        let urlSession = URLSession(configuration: config)
        let api = RestApi(urlSession: urlSession)

        let data = MockData.incorrectData

        MockURLProtocol.requestHandler = { request in
             guard let url = request.url else {
               throw ApiError.invalidUrl
             }

             let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
             return (response, data)
           }

        //when
        api.getExchangeRates(for: ["USD:EUR"]) { currencyPairs, error in
            expectation.fulfill()
            currencyEmpty = currencyPairs?.isEmpty ?? true
            apiError = error
        }

        wait(for: [expectation], timeout: 3)

        //then
        XCTAssertTrue(currencyEmpty, "List of currency pairs should be empty")
        XCTAssertNotNil(apiError)
    }

}
