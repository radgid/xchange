//
//  xChangeTests.swift
//  xChangeTests
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import XCTest
@testable import xChange

class IntegrationTests: XCTestCase {

    fileprivate var urlSession: URLSession?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        urlSession =  URLSession.shared
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExchangeRatesOneCurrencyPairValid() {

        let expectation = XCTestExpectation(description: "Test Valid Currency Pairs data")
        var currencyCount = 0
        var apiError: Error?
        RestApi().getExchangeRates(for: ["EURGBP"]) { (currencyPairs, error) in
            expectation.fulfill()
            currencyCount = currencyPairs?.count ?? 0
            apiError = error

        }

        wait(for: [expectation], timeout: 3)

        XCTAssert(currencyCount == 1, "List of currency pairs is empty")
        XCTAssertNil(apiError)
    }

    func testExchangeRatesMultiCurrencyPairsValid() {

        let expectation = XCTestExpectation(description: "Test Valid Currency Pairs data")
        var currencyCount = 0
        var apiError: Error?
        RestApi().getExchangeRates(for: ["EURGBP", "USDGBP"]) { (currencyPairs, error) in
            expectation.fulfill()
            currencyCount = currencyPairs?.count ?? 0
            apiError = error

        }
        wait(for: [expectation], timeout: 3)

        XCTAssert(currencyCount == 2, "List of currency pairs is empty")
        XCTAssertNil(apiError)
    }
    func testExchangeRatesOneCurrencyPairInvalid() {

        let expectation = XCTestExpectation(description: "Test Invalid Currency Pair data")
        var apiError: Error?
        RestApi().getExchangeRates(for: ["EURGBP   "]) { (_, error) in
            expectation.fulfill()
            apiError = error

        }
        wait(for: [expectation], timeout: 3)

        XCTAssert(apiError?.localizedDescription == ApiError.invalidUrl.localizedDescription, "Invalid URL must be handled")
    }
}
