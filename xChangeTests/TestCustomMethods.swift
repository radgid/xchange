//
//  xChangeTests.swift
//  xChangeTests
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import XCTest
@testable import xChange

class TestCustomMethods: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCurrencyStringNotEmpty() {
        let currenciesString = Configuration.currencies
        XCTAssert(currenciesString.count > 0)
    }

    func testCurrencyArrayNilInitString() {
        let currenciesString: String? = nil
        let currencies = Currency(currenciesString)

        XCTAssert(currencies.count == 0)
    }

    func testCurrencyArrayEmptyInitString() {
        let currenciesString: String? = ""
        let currencies = Currency(currenciesString)

        XCTAssert(currencies.count == 0)
    }

    func testCurrencyArrayValidInitString() {
        let currenciesString = Configuration.currencies
        guard currenciesString.count > 0 else {
            XCTFail("Currencies list needs to be present in the App Configuration")
            return
        }

        let currencies = Currency(currenciesString)

        XCTAssert(currencies.count > 0)
    }

    func testUserPreferencesCurrencyPairRemoveCurrencyPair() {
        let userPreferences = UserPreferences()
        userPreferences.setCurrencyPair("GBP", "USD")
        guard let preferenceCurrencyPair = userPreferences.getCurrencyPairs() else {
            XCTFail("User Preference Currency Pair not stored")
            return
        }
        userPreferences.removeCurrencyPair("GBP", "USD")
        let removedPreferenceCurrencyPair = userPreferences.getCurrencyPairs()
        XCTAssertNotNil(preferenceCurrencyPair)
        XCTAssert(!(removedPreferenceCurrencyPair?.contains("GBPUSD") ?? false))
    }

    func testUserPreferencesCurrencyPairStoreOne() {
        let userPreferences = UserPreferences()
        userPreferences.removeCurrencyPairs()
        userPreferences.setCurrencyPair("GBP", "USD")
        guard let preferenceCurrencyPair = userPreferences.getCurrencyPairs() else {
            XCTFail("User Preference Currency Pair not stored")
            return
        }

        XCTAssertNotNil(preferenceCurrencyPair)
        XCTAssert(preferenceCurrencyPair.contains("GBPUSD"))
        XCTAssert(preferenceCurrencyPair.count == 1 )
    }

    func testUserPreferencesCurrencyPairRemoveAllCurrencyPairs() {
        let userPreferences = UserPreferences()
        userPreferences.setCurrencyPair("GBP", "USD")
        guard let preferenceCurrencyPair = userPreferences.getCurrencyPairs() else {
            XCTFail("User Preference Currency Pair not stored")
            return
        }
        userPreferences.removeCurrencyPairs()
        let nullifiedPreferenceCurrencyPair = userPreferences.getCurrencyPairs()
        XCTAssertNotNil(preferenceCurrencyPair)
        XCTAssertNil(nullifiedPreferenceCurrencyPair)
    }

    func testUserPreferencesCurrencyPairStoreMore() {
        let userPreferences = UserPreferences()
        userPreferences.removeCurrencyPairs()
        userPreferences.setCurrencyPair("GBP", "USD")
        userPreferences.setCurrencyPair("GBP", "EUR")
        guard let preferenceCurrencyPair = userPreferences.getCurrencyPairs() else {
            XCTFail("User Preference Currency Pair not stored")
            return
        }

        XCTAssertNotNil(preferenceCurrencyPair)
        XCTAssert(preferenceCurrencyPair.contains("GBPUSD"))
        XCTAssert(preferenceCurrencyPair.contains("GBPEUR"))
    }
    func testUserPreferencesCurrencyPairMoreRemoveOne() {
        let userPreferences = UserPreferences()
        userPreferences.removeCurrencyPairs()
        userPreferences.setCurrencyPair("GBP", "USD")
        userPreferences.setCurrencyPair("GBP", "EUR")
        guard let preferenceCurrencyPair = userPreferences.getCurrencyPairs() else {
            XCTFail("User Preference Currency Pair not stored")
            return
        }

        XCTAssertNotNil(preferenceCurrencyPair)
        XCTAssert(preferenceCurrencyPair.contains("GBPUSD"))
        XCTAssert(preferenceCurrencyPair.contains("GBPEUR"))

        userPreferences.removeCurrencyPair("GBP", "USD")
        let removedPreferenceCurrencyPair = userPreferences.getCurrencyPairs()
        XCTAssert(!(removedPreferenceCurrencyPair?.contains("GBPUSD") ?? false))
        XCTAssert((removedPreferenceCurrencyPair?.contains("GBPEUR") ?? false))
    }
    func testUserPreferencesCurrencyPairMoreRemoveOneByKey() {
        let userPreferences = UserPreferences()
        userPreferences.removeCurrencyPairs()
        userPreferences.setCurrencyPair("GBP", "USD")
        userPreferences.setCurrencyPair("GBP", "EUR")
        guard let preferenceCurrencyPair = userPreferences.getCurrencyPairs() else {
            XCTFail("User Preference Currency Pair not stored")
            return
        }

        XCTAssertNotNil(preferenceCurrencyPair)
        XCTAssert(preferenceCurrencyPair.contains("GBPUSD"))
        XCTAssert(preferenceCurrencyPair.contains("GBPEUR"))

        userPreferences.removeCurrencyPair(key: "GBPUSD")
        let removedPreferenceCurrencyPair = userPreferences.getCurrencyPairs()
        XCTAssert(!(removedPreferenceCurrencyPair?.contains("GBPUSD") ?? false))
        XCTAssert((removedPreferenceCurrencyPair?.contains("GBPEUR") ?? false))
    }
    func testUserPreferencesCurrencyPairMoreRemoveOneByInvalidKey() {
        let userPreferences = UserPreferences()
        userPreferences.removeCurrencyPairs()
        userPreferences.setCurrencyPair("GBP", "USD")
        userPreferences.setCurrencyPair("GBP", "EUR")
        guard let preferenceCurrencyPair = userPreferences.getCurrencyPairs() else {
            XCTFail("User Preference Currency Pair not stored")
            return
        }

        XCTAssertNotNil(preferenceCurrencyPair)
        XCTAssert(preferenceCurrencyPair.contains("GBPUSD"))
        XCTAssert(preferenceCurrencyPair.contains("GBPEUR"))

        userPreferences.removeCurrencyPair(key: "GBPUS")
        let removedPreferenceCurrencyPair = userPreferences.getCurrencyPairs()
        XCTAssert((removedPreferenceCurrencyPair?.contains("GBPUSD") ?? false))
        XCTAssert((removedPreferenceCurrencyPair?.contains("GBPEUR") ?? false))
    }
    func testUserPreferencesCurrencyPairOrder() {
        let userPreferences = UserPreferences()
        userPreferences.removeCurrencyPairs()
        userPreferences.setCurrencyPair("GBP", "USD")
        userPreferences.setCurrencyPair("GBP", "EUR")
        userPreferences.setCurrencyPair("AUD", "EUR")
        guard let preferenceCurrencyPair = userPreferences.getCurrencyPairs() else {
            XCTFail("User Preference Currency Pair not stored")
            return
        }
        XCTAssertNotNil(preferenceCurrencyPair)
        XCTAssert(preferenceCurrencyPair[0] == "AUDEUR")
        XCTAssert(preferenceCurrencyPair[1] == "GBPEUR")
        XCTAssert(preferenceCurrencyPair[2] == "GBPUSD")
    }
}
