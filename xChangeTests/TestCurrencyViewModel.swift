//
//  TestCurrencyViewModel.swift
//  xChangeTests
//
//  Created by Danko, Radoslav on 20/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import XCTest
@testable import xChange

class TestCurrencyViewModel: XCTestCase {

    override func setUp() {
        UserPreferences().removeCurrencyPairs()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInitEmptyCurrencyEmptyPair() {
        let vm = CurrencyViewModel("", currencyPair: "")
        XCTAssert(vm.currencyCode == "")
        XCTAssert(vm.currencyDescription == "")
    }

    func testInitValidCurrencyValidPair() {
        let vm = CurrencyViewModel("GBP", currencyPair: "GBPUSD")
        XCTAssert(vm.currencyCode == "GBP")
        XCTAssert(vm.currencyDescription == Locale.current.localizedString(forCurrencyCode: "GBP") ?? "")
    }

    func testIsSelectableFalse() {
        let vm = CurrencyViewModel("GBP", currencyPair: "GBPUSD")
        XCTAssertFalse(vm.isSelectable)

    }
    func testIsSelectableTrue() {
        let vm = CurrencyViewModel("AUD", currencyPair: "GBPUSD")
        XCTAssertTrue(vm.isSelectable)

    }
    func testIsSelectableTrueUserPreferencesCurrencyPairDoesNotContainCurrency() {
        UserPreferences().setCurrencyPair("GBP", "EUR")
        let vm = CurrencyViewModel("AUD", currencyPair: "GBPUSD")
        XCTAssertTrue(vm.isSelectable)

    }

    func testIsSelectableFalseUserPreferencesCurrencyPairContainsToCurrency() {
        let userPreferences = UserPreferences()
        userPreferences.removeCurrencyPairs()
        userPreferences.setCurrencyPair("AUD", "IDR")
        let vm = CurrencyViewModel("IDR", currencyPair: "AUD")
        XCTAssertFalse(vm.isSelectable)

    }
}
