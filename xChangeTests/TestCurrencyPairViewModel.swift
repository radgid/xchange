//
//  TestCurrencyPairViewModel.swift
//  xChangeTests
//
//  Created by Danko, Radoslav on 20/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import XCTest
@testable import xChange

class TestCurrencyPairViewModel: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInitEmptyCurrencyPair() {
        let vm = CurrencyPairViewModel(currencyPair: [:])
        XCTAssert(vm.fromCurrency == "")
    }
    func testInitValidCurrencyPairFromCurrency() {
        let vm = CurrencyPairViewModel(currencyPair: ["GBPUSD": 1.1234])
        XCTAssert(vm.fromCurrency == "1 GBP", "From Currency incorrect")
    }
    func testInitValidCurrencyPairToCurrency() {
        let vm = CurrencyPairViewModel(currencyPair: ["GBPUSD": 1.1234])
        XCTAssert(vm.toCurrency == "1.1234", "Rate string incorrect")
    }
    func testInitValidCurrencyPairFromCurrencyDetail() {
        let vm = CurrencyPairViewModel(currencyPair: ["GBPUSD": 1.1234])
        XCTAssert(vm.fromCurrencyDetail == Locale.current.localizedString(forCurrencyCode: "GBP"), "From Currency description incorrect")
    }
    func testInitValidCurrencyPairToCurrencyDetail() {
        let vm = CurrencyPairViewModel(currencyPair: ["GBPUSD": 1.1234])
        XCTAssert(vm.toCurrencyDetail == "\(Locale.current.localizedString(forCurrencyCode: "USD") ?? "") . USD", "To Currency description incorrect")
    }
    func testInitInvalidCurrencyPairFromCurrency() {
        let vm = CurrencyPairViewModel(currencyPair: ["BLADOH": 1.1234])
        XCTAssert(vm.fromCurrency == "", "From Currency incorrect")
    }
    func testInitInvalidCurrencyPairFromCurrencyDetail() {
        let vm = CurrencyPairViewModel(currencyPair: ["BLADOH": 1.1234])
        XCTAssert(vm.fromCurrencyDetail == "", "From Currency description incorrect")
    }
    func testInitInvalidCurrencyPairToCurrencyDetail() {
        let vm = CurrencyPairViewModel(currencyPair: ["BLADOH": 1.1234])
        XCTAssert(vm.toCurrencyDetail == "", "To Currency description incorrect")
    }
}
