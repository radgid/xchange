//
//  TestCurrencyPairsViewModel.swift
//  xChangeTests
//
//  Created by Danko, Radoslav on 20/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import XCTest
@testable import xChange

class TestCurrencyPairsViewModel: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testsInitEmptyCurrencyPairs() {
        let currencyPairs: CurrencyPair = [:]
        let vm = CurrencyPairsViewModel(currencyPairs: currencyPairs)
        XCTAssertTrue(vm.currencyPairs.count == 0)
    }
    func testsInitValidCurrencyPairs() {
        let currencyPairs: CurrencyPair = ["GBPUSD": 1.2345, "GBPEUR": 1.1231]
        let vm = CurrencyPairsViewModel(currencyPairs: currencyPairs)
        XCTAssertTrue(vm.currencyPairs.count == 2)
    }

    func testCurrenciesListCountMatchesNumberOfRows() {
        let currencyPairs: CurrencyPair = ["GBPUSD": 1.2345, "GBPEUR": 1.1231]
        let vm = CurrencyPairsViewModel(currencyPairs: currencyPairs)
        XCTAssert(vm.numberOfRows == currencyPairs.count, "Currencies Pairs count must match array count")
    }

    func testCurrenciesNumberOfSectionsIsOne() {
        let currencyPairs: CurrencyPair = ["GBPUSD": 1.2345, "GBPEUR": 1.1231]
        let vm = CurrencyPairsViewModel(currencyPairs: currencyPairs)
        XCTAssert(vm.numberOfSections == 1, "Currencies Pairs in one section only")
    }

    func testsInitValidCurrencyPairsUserPreferenceEmptyExchangeRateEmpty() {
        let userPreferences = UserPreferences()
        userPreferences.removeCurrencyPairs()
        let currencyPairs: CurrencyPair = ["GBPUSD": 1.2345, "GBPEUR": 1.1231]
        let vm = CurrencyPairsViewModel(currencyPairs: currencyPairs)
        let pair = vm.exchangeRate(for: IndexPath(row: 1, section: 0))
        XCTAssertNil(pair)
    }

    func testsInitValidCurrencyPairsUserPreferenceValidExchangeRateValid() {
        let userPreferences = UserPreferences()
        userPreferences.removeCurrencyPairs()
        userPreferences.setCurrencyPair("GBP", "USD")
        userPreferences.setCurrencyPair("GBP", "EUR")
        let currencyPairs: CurrencyPair = ["GBPUSD": 1.2345, "GBPEUR": 1.1231]
        let vm = CurrencyPairsViewModel(currencyPairs: currencyPairs)
        let pair = vm.exchangeRate(for: IndexPath(row: 0, section: 0))
        XCTAssert(pair == ["GBPEUR": 1.1231])
    }
}
