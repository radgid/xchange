import UIKit

let pairs = ["abcdef", "ghijkl"]

var concatenated = pairs.reduce(into: "?") { (result, next) in
    result = "\(result)pairs=\(next)&"
}

print(concatenated)
print(concatenated.prefix(concatenated.count-1))
