//
//  RestApi.swift
//  xChange
//
//  Created by Danko, Radoslav on 18/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

typealias CurrencyPairCompletion = (_ result: CurrencyPair?, _ error: Error?) -> Void

enum ApiError: String, LocalizedError {
    case invalidUrl = "Invalid URL"
    case invalidResponse = "Invalid response"
}

struct RestApi {

    private let urlSession: URLSession

    init(urlSession: URLSession = URLSession.shared) {
      self.urlSession = urlSession
    }

    fileprivate func exchangeRatesUrl(for pairs: [String]) -> URL? {
        let endpoint = Configuration.Api.currencyPair
        let queryPath = pairs.reduce(into: "?") { (result, next) in
           result = "\(result)pairs=\(next)&"
        }
        return URL(string: "\(endpoint)\(queryPath.prefix(queryPath.count-1))")
    }

    public func getExchangeRates(for currencyPairs: [String], completion:@escaping CurrencyPairCompletion) {

        guard let url = exchangeRatesUrl(for: currencyPairs) else {
            DispatchQueue.main.async {
                completion(nil, ApiError.invalidUrl)
            }
            return
        }
        let task = urlSession.currencyPairTask(with: url) { (result, _, error) in
            DispatchQueue.main.async {
                completion(result, error)
            }
        }
        task.resume()

    }
}
