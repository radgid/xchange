//
//  UserPreferences.swift
//  xChange
//
//  Created by Danko, Radoslav on 17/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

/// Store user data/settings
class UserPreferences {
    private let defaults = UserDefaults.standard

    func removeCurrencyPair(_ fromCurrency: String, _ toCurrency: String) {
        var pairs = defaults.value(forKey: UserDefaults.Key.CurrencyPair) as? [String] ?? []
        pairs.removeAll { (value) -> Bool in
            return "\(fromCurrency)\(toCurrency)" == value
        }
        defaults.set(pairs, forKey: UserDefaults.Key.CurrencyPair)
        defaults.synchronize()
    }

    func removeCurrencyPair(key: String) {
        guard key.count == 6 else {
            return
        }

        let fromCurrency = String(key.prefix(3))
        let toCurrency = String(key.suffix(3))

        removeCurrencyPair(fromCurrency, toCurrency)
    }

    func removeCurrencyPairs() {
        defaults.removeObject(forKey: UserDefaults.Key.CurrencyPair)
        defaults.synchronize()
    }

    func setCurrencyPair(_ fromCurrency: String, _ toCurrency: String) {
        var pairs = defaults.value(forKey: UserDefaults.Key.CurrencyPair) as? [String] ?? []
        guard !pairs.contains("\(fromCurrency)\(toCurrency)") else {
            return
        }
        pairs.insert("\(fromCurrency)\(toCurrency)", at: 0) //put user selected currency pair on top of previously set
        defaults.set(pairs, forKey: UserDefaults.Key.CurrencyPair)
        defaults.synchronize()
    }

    func getCurrencyPairs() -> [String]? {
        return defaults.value(forKey: UserDefaults.Key.CurrencyPair) as? [String]
    }
}
