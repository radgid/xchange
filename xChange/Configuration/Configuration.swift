//
//  File.swift
//  xChange
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

/// Application global configuration
struct Configuration {
    struct Api {
        /// URL to get the currency pairs
        static var currencyPair: String {
            return "https://europe-west1-revolut-230009.cloudfunctions.net/revolut-ios"
        }
    }
    /// Available currencies
    static var currencies: String { return """
        [
        "AUD",
        "BGN",
        "BRL",
        "CAD",
        "CHF",
        "CNY",
        "CZK",
        "DKK",
        "EUR",
        "GBP",
        "HKD",
        "HRK",
        "HUF",
        "IDR",
        "ILS",
        "INR",
        "ISK",
        "JPY",
        "KRW",
        "MXN",
        "MYR",
        "NOK",
        "NZD",
        "PHP",
        "PLN",
        "RON",
        "RUB",
        "SEK",
        "SGD",
        "THB",
        "USD",
        "ZAR"
        ]
        """
    }
}
