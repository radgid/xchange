//
//  URLSession+Extension.swift
//  xChange
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

extension URLSession {

    /// Create the Swift object from the data returned by API call
    ///
    /// - Parameters:
    ///   - url: API endpoint
    ///   - completionHandler: API call completion
    /// - Returns: Data Task
    public func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }

            do {
                let decoded = try Json().decoder.decode(T.self, from: data)
 				completionHandler(decoded, response, nil)
            } catch {
            	completionHandler(nil, response, ApiError.invalidResponse)
            }
//            completionHandler(try? Json().decoder.decode(T.self, from: data), response, nil)
        }
    }
}
