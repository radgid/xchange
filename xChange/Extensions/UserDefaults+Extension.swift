//
//  UserDefaults+Extension.swift
//  xChange
//
//  Created by Danko, Radoslav on 17/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation
extension UserDefaults {
    struct Key {
        static var CurrencyPair: String {
            return "currencyPair"
        }
    }
}
