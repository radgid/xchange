//
//  CurrencyPairsViewController.swift
//  xChange
//
//  Created by Danko, Radoslav on 17/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

/// Currency Pairs data
struct CurrencyPairsViewModel {
    let currencyPairs: CurrencyPair
    private var userCurrencyPairs = UserPreferences().getCurrencyPairs()

    init(currencyPairs: CurrencyPair) {
        self.currencyPairs = currencyPairs
    }

    var numberOfRows: Int {
        return currencyPairs.count
    }

    var numberOfSections: Int {
        return 1
    }

    func exchangeRate(for indexPath: IndexPath) -> CurrencyPair? {
        if let userCurrencyPairs = userCurrencyPairs, !userCurrencyPairs.isEmpty, !currencyPairs.isEmpty {
            let currencyPair = userCurrencyPairs[indexPath.row]
            return currencyPairs.filter({$0.key == currencyPair})
        }
        return nil
    }

    func refresh(completion:@escaping CurrencyPairCompletion) {
        //Do not ask for exchange rates if previous response not received yet
        RestApi().getExchangeRates(for: UserPreferences().getCurrencyPairs() ?? []) { (pairs, error) in

            guard error == nil else {
                completion(nil, error)
                return
            }

            /// In the case the previous calls yielded results then do not reset the viewModel - keep old data to show at least something
            guard let pairs = pairs else {
                completion(nil, error)
                return
            }
            completion(pairs, error)
        }
    }
}
