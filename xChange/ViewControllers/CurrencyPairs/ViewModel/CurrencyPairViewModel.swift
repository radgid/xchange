//
//  CurrencyPairViewModel.swift
//  xChange
//
//  Created by Danko, Radoslav on 18/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

struct CurrencyPairViewModel: CurrencyPairRepresentable {
    fileprivate var currencyPair: CurrencyPair? {
        didSet {
            if let exchangeRate = currencyPair?.first, exchangeRate.key.count == 6 {
                fromCurrencyCode = String(exchangeRate.key.prefix(3))
                toCurrencyCode = String(exchangeRate.key.suffix(3))
            }

        }
    }
    fileprivate var fromCurrencyCode: String? {
        didSet {
            if let fromCurrencyCode = fromCurrencyCode, fromCurrencyCode.count == 3 {
                fromCurrencyDetail = Locale.current.localizedString(forCurrencyCode: fromCurrencyCode) ?? ""
                fromCurrency = fromCurrencyDetail.count > 0 ? "1 \(fromCurrencyCode)" : ""
            }
        }
    }

    fileprivate var toCurrencyCode: String? {
        didSet {
            if let toCurrencyCode = toCurrencyCode, toCurrencyCode.count == 3 {
                if let currencyDescription = Locale.current.localizedString(forCurrencyCode: toCurrencyCode) {
                    toCurrencyDetail = currencyDescription.count > 0 ? "\(currencyDescription) . \(toCurrencyCode)" : ""
                    if let exchangeRate = currencyPair?.first {
                        let formatter = NumberFormatter()
                        formatter.locale = Locale.current
                        formatter.numberStyle = .decimal
                        formatter.minimumFractionDigits = 4
                        toCurrency = formatter.string(from: NSNumber(value: exchangeRate.value)) ?? ""
                    }
                }

            }
        }
    }

    init(currencyPair: CurrencyPair) {
        defer {
            self.currencyPair = currencyPair
        }
    }

    var fromCurrency: String  = ""

    var toCurrency: String = ""

    var fromCurrencyDetail: String = ""

    var toCurrencyDetail: String = ""

}
