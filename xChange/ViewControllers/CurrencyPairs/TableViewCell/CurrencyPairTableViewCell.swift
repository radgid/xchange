//
//  CurrencyPairTableViewCell.swift
//  xChange
//
//  Created by Danko, Radoslav on 18/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import UIKit

class CurrencyPairTableViewCell: UITableViewCell, CurrencyCellProtocol {
    static var reuseIdentifier: String {
        return "currencyPairCell"
    }

    @IBOutlet private weak var fromCurrency: UILabel!
    @IBOutlet private weak var toCurrency: UILabel!
    @IBOutlet private weak var fromCurrencyDetail: UILabel!
    @IBOutlet private weak var toCurrencyDetail: UILabel!

    func configure(withViewModel viewModel: AnyObject) {
        guard let viewModel = viewModel as? CurrencyPairRepresentable else {
            return
        }

        fromCurrency.text = viewModel.fromCurrency
        let formattedToCurrency = NSMutableAttributedString(string: viewModel.toCurrency)
        let font = UIFont.boldSystemFont(ofSize: 17)
        if viewModel.toCurrency.count > 2 {
            formattedToCurrency.addAttribute(NSAttributedString.Key.font, value: font, range: NSRange(location: viewModel.toCurrency.count - 2, length: 2))
            toCurrency.attributedText = formattedToCurrency
        } else {
            toCurrency.text = viewModel.toCurrency
        }
        fromCurrencyDetail.text = viewModel.fromCurrencyDetail
        toCurrencyDetail.text = viewModel.toCurrencyDetail

    }

}
