//
//  File.swift
//  xChange
//
//  Created by Danko, Radoslav on 18/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

/// The protocol defining attributes for Currency Pair cell
protocol CurrencyPairRepresentable {
    var fromCurrency: String {get}
    var toCurrency: String {get}
    var fromCurrencyDetail: String {get}
    var toCurrencyDetail: String {get}
}
