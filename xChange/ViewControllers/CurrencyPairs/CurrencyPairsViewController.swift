//
//  MainViewController.swift
//  xChange
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import UIKit

class CurrencyPairsViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var helpLabel: UILabel!
    @IBOutlet private weak var plusButton: UIButton!
    @IBOutlet private weak var controlStackView: UIStackView!
    @IBOutlet private weak var controlStackViewCenterY: NSLayoutConstraint!
    @IBOutlet private weak var plusButtonHeight: NSLayoutConstraint!
    @IBOutlet private weak var controlStackViewHeight: NSLayoutConstraint!
    private var deletedRow: Int?
    private var refreshTimer: Timer?
    private var apiResponseReceived: Bool = true

    private var viewModel: CurrencyPairsViewModel? {
        didSet {
            updateView(oldCurrencyCount: oldValue?.currencyPairs.count, newCurrencyCount: viewModel?.currencyPairs.count)
        }
    }

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CurrencyPairsViewModel(currencyPairs: [:])
        setUpTimer()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.refreshTimer?.invalidate()
    }

    deinit {
        self.refreshTimer?.invalidate()
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        tableView.setEditing(false, animated: true)
    }

    @IBAction func unwindToCurrencyPairs(_ unwindSegue: UIStoryboardSegue) {
        refreshData()

    }

    // MARK: - Custom Methods

    /// Simple timer
    private func setUpTimer() {
        self.refreshTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(refreshData), userInfo: nil, repeats: true)
    }
    /// Animate buttons in the case the Currency was added/removed
    private func animateButtons() {
        /// Exchange Rates list exists so shrink the "Add" button view and expand the tableView
        if let viewModel = viewModel, viewModel.numberOfRows > 0 {
            guard self.helpLabel.isHidden == false else {
                return
            }
            controlStackView.translatesAutoresizingMaskIntoConstraints = false
            self.helpLabel.isHidden = true
            self.controlStackView.axis = .horizontal
            self.controlStackView.distribution = .fill

            let controlStackViewY = (self.view.center.y - 100) * -1

            UIView.animate(withDuration: 0.3, animations: {
                self.plusButtonHeight.constant = 50
                self.controlStackViewHeight.constant = 90
                self.controlStackViewCenterY.constant = controlStackViewY
                self.view.layoutIfNeeded()
            }) { (_) in

            }

        } else {
            /// Exchange Rates list is empty so expand the "Add" button view and hide the tableView
            guard self.helpLabel.isHidden == true else {
                return
            }
            controlStackView.translatesAutoresizingMaskIntoConstraints = false
            helpLabel.alpha = 0
            self.helpLabel.isHidden = false
            self.controlStackView.axis = .vertical
            self.controlStackView.distribution = .equalSpacing
            UIView.animate(withDuration: 0.3, animations: {
                self.controlStackViewHeight.constant = 146
                self.plusButtonHeight.constant = 60
                self.controlStackViewCenterY.constant = 0
                self.helpLabel.alpha = 1
                self.view.layoutIfNeeded()

            }) { (_) in
            }

        }
        //Round the plus button as the graphics is square
        plusButton.layer.cornerRadius = plusButtonHeight.constant/2
    }

    /// Table updates
    private func updateTable(oldCurrencyCount: Int, newCurrencyCount: Int) {

        if oldCurrencyCount == 0 {
            //Screen just loaded so present all remembered data
            tableView.reloadData()
            //update buttons if first exchange rate was addded
            animateButtons()
        } else {
            tableView.performBatchUpdates({
                //new Exchange Rate added - from unwind seque

                if newCurrencyCount > oldCurrencyCount {
                    tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .bottom)
                } else if newCurrencyCount > 0 && newCurrencyCount  == oldCurrencyCount {
                    //No change in exchange rates count, refresh the data
                    var reloadPaths: [IndexPath] = []
                    for ind in 0 ... (newCurrencyCount )-1 {
                        reloadPaths.append(IndexPath(row: ind, section: 0))
                    }
                    tableView.reloadRows(at: reloadPaths, with: .none)
                } else {
                    if let deletedRow = deletedRow {
                        tableView.deleteRows(at: [IndexPath(row: deletedRow, section: 0)], with: .automatic)
                        self.deletedRow = nil
                    }
                }
            }, completion: nil)

            //update buttons if last exchange rate was removed
            if oldCurrencyCount == 1 && newCurrencyCount == 0 {
                animateButtons()
            }
        }
    }

    private func updateView(oldCurrencyCount: Int?, newCurrencyCount: Int?) {
        updateTable(oldCurrencyCount: oldCurrencyCount ?? 0, newCurrencyCount: newCurrencyCount ?? 0)
    }

    /// Simple Alert
    ///
    /// - Parameter message: Alert message
    private func showError(message: String?) {

        let alert = UIAlertController.init(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            self.setUpTimer()
        }))
        self.refreshTimer?.invalidate()
        self.present(alert, animated: true, completion: nil)

    }

    /// Refresh Exchange Rates
    @objc private func refreshData() {
        if tableView.isEditing {
            tableView.setEditing(false, animated: true)
        }
        guard apiResponseReceived == true else {
            return
        }
        apiResponseReceived = false

        viewModel?.refresh(completion: { [weak self] (pairs, error) in
            if let pairs = pairs {
                self?.viewModel = CurrencyPairsViewModel(currencyPairs: pairs)
            }
            self?.apiResponseReceived = true
            if let refreshTimer = self?.refreshTimer, !refreshTimer.isValid {
                self?.setUpTimer()
            }
            guard error == nil else {
                self?.showError(message: error?.localizedDescription)
                return
            }

        })
    }

    // MARK: - Actions
    @IBAction func onTapAddCurrencyPair(_ sender: Any) {
        performSegue(withIdentifier: "selectCurrencySegue", sender: self)
    }
}

// MARK: - Table View Delegate
extension CurrencyPairsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }
        return viewModel.numberOfRows
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }
        return viewModel.numberOfSections
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let currencyPair = viewModel?.exchangeRate(for: indexPath) {
            let viewModelCurrencyPair = CurrencyPairViewModel(currencyPair: currencyPair)
            if let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyPairTableViewCell.reuseIdentifier,
                                                        for: indexPath) as? CurrencyPairTableViewCell {
                cell.configure(withViewModel: viewModelCurrencyPair as AnyObject)
                return cell
            }
            return UITableViewCell()
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        self.refreshTimer?.invalidate()
    }
    func tableView(_ tableView: UITableView, didEndEditingRowAt indexPath: IndexPath?) {
        refreshData()
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let currencyPair = viewModel?.exchangeRate(for: indexPath) {
                deletedRow = indexPath.row
                if let key = currencyPair.first?.key {
                    UserPreferences().removeCurrencyPair(key: key)
                    refreshData()
                }
            }
        }
    }

}
