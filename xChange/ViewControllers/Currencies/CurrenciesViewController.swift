//
//  ViewController.swift
//  xChange
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import UIKit

class CurrenciesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    fileprivate var fromCurrency: String? {
        didSet {
            viewModel?.fromCurrency = fromCurrency
        }
    }
    fileprivate var toCurrency: String? {
        didSet {
            viewModel?.toCurrency = toCurrency
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        viewModel = CurrenciesViewModel()
    }

    fileprivate var viewModel: CurrenciesViewModel? {
        didSet {
            updateView()
        }
    }

    // MARK: Custom Methods
    fileprivate func reloadRowForCurrency(_ currency: String?) {
        if let currency = currency {
            if let indexPath = viewModel?.indexPath(for: currency) {
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
        }
    }
    fileprivate func updateView() {

        tableView.performBatchUpdates({
            //Disabling currency rows :
            // - rows with currencies which are currently selected "fromCurrency" "toCurrency"
            // - the currency already present in the stored currency pairs list
            reloadRowForCurrency(fromCurrency)
            reloadRowForCurrency(toCurrency)
            _ = UserPreferences().getCurrencyPairs()?.filter({ (pair) -> Bool in
                String(pair.prefix(3)) == fromCurrency
            }).map({ (pair) -> Void in
                reloadRowForCurrency(String(pair.suffix(3)))
            })

        }) { (_) in

        }
    }
}

// MARK: Table View Delegate
extension CurrenciesViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }
        return viewModel.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else {
            return 0
        }
        return viewModel.numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if let currencyCode = viewModel?.currency(for: indexPath) {
            let currencyViewModel = CurrencyViewModel(currencyCode, currencyPair: viewModel?.currencyPair() ?? "")
            if let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyTableViewCell.reuseIdentifier, for: indexPath) as? CurrencyTableViewCell {
                cell.configure(withViewModel: currencyViewModel as AnyObject)
                return cell
            }
            return UITableViewCell()
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let currencyCode = viewModel?.currency(for: indexPath) {
            if fromCurrency == nil || fromCurrency?.count == 0 {
                fromCurrency = currencyCode
            } else {
                toCurrency = currencyCode
            }
        }

        if let fromCurrency = fromCurrency, fromCurrency.count > 0, let toCurrency = toCurrency, toCurrency.count > 0 {
            UserPreferences().setCurrencyPair(fromCurrency, toCurrency)
            performSegue(withIdentifier: "unwindToCurrencyPairs", sender: self)
        }
    }

}
