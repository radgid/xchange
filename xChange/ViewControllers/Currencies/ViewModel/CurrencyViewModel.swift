//
//  CurrencyViewModel.swift
//  xChange
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

/// Currency data
struct CurrencyViewModel: CurrencyRepresentable {
    var currencyCode: String
    fileprivate var currencyPair: String
    init(_ currencyCode: String, currencyPair: String) {
        self.currencyCode = currencyCode.uppercased()
        self.currencyPair = currencyPair.uppercased()
    }

    var currencyDescription: String {
        return Locale.current.localizedString(forCurrencyCode: currencyCode) ?? ""
    }

    /// The currency selectable flag is set based on the currently selected currency and related currency pairs selected previously
    var isSelectable: Bool {
       return !(currencyPair.contains(currencyCode) ||
        (currencyPair.count > 0 && UserPreferences().getCurrencyPairs()?.contains { pair in
            return String(pair.suffix(3)) == currencyCode
            } ?? false))

    }

}
