//
//  ViewModel.swift
//  xChange
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

/// Currencies data
struct CurrenciesViewModel {

    let currencies: Currency
    var fromCurrency: String?
    var toCurrency: String?

    init() {
        self.currencies = Currency(Configuration.currencies).sorted()
    }

    var numberOfRows: Int {
        return currencies.count
    }

    var numberOfSections: Int {
        return 1
    }

    func indexPath(for currency: String) -> IndexPath? {
        if let index = currencies.firstIndex(of: currency) {
            return IndexPath(row: index, section: 0)
        }

        return nil
    }

    func currency(for indexPath: IndexPath) -> String? {
        guard indexPath.row < currencies.count else {
            return nil
        }
        return currencies[indexPath.row]
    }

    func currencyPair() -> String {
        return "\(fromCurrency ?? "")\(toCurrency ?? "")"
    }
}
