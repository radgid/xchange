//
//  CurrencyRepresentable.swift
//  xChange
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

/// The protocol defining attributes for Currency cell
protocol CurrencyRepresentable {
    var currencyCode: String {get}
    var currencyDescription: String {get}
    var isSelectable: Bool {get}
}
