//
//  CurrencyTableViewCell.swift
//  xChange
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import UIKit

protocol CurrencyCellProtocol {
    static var reuseIdentifier: String {get}
    func configure(withViewModel viewModel: AnyObject)
}

class CurrencyTableViewCell: UITableViewCell, CurrencyCellProtocol {

    class var reuseIdentifier: String {
        return "currencyCell"
    }

    @IBOutlet weak var flag: UIImageView!
    @IBOutlet weak var currencyCode: UILabel!
    @IBOutlet weak var currencyDescription: UILabel!

    private var viewModel: CurrencyRepresentable?

    fileprivate var isSelectable: Bool? {
        didSet {
            isUserInteractionEnabled = isSelectable ?? true
            if isUserInteractionEnabled {
                UIView.animate(withDuration: 0.3) {
                    self.contentView.alpha = 1.0
                }

            } else {
                UIView.animate(withDuration: 0.3) {
                    self.contentView.alpha = 0.3
                }
            }
        }
    }

    func configure(withViewModel viewModel: AnyObject) {

        guard let viewModel = viewModel as? CurrencyRepresentable else {
            return
        }

        self.viewModel = viewModel
        currencyCode.text = viewModel.currencyCode
        currencyDescription.text = viewModel.currencyDescription
        flag.image = UIImage(named: viewModel.currencyCode)

        isSelectable = viewModel.isSelectable

    }
}
