//
//  CurrencyPair.swift
//  xChange
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

typealias CurrencyPair = [String: Double]

// MARK: - URLSession response handlers
extension URLSession {
    /// Obtain the Currency pairs
    ///
    /// - Parameters:
    ///   - url: Currency Pairs API
    ///   - completionHandler: API call completion
    /// - Returns: Data Task
    func currencyPairTask(with url: URL, completionHandler: @escaping (CurrencyPair?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}
