//
//  Currency.swift
//  xChange
//
//  Created by Danko, Radoslav on 16/05/2019.
//  Copyright © 2019 Danko, Radoslav. All rights reserved.
//

import Foundation

typealias Currency = [String]

// MARK: - Helpers
extension Currency {
    init(_ string: String?) {
        guard let string = string, string.count > 0 else {
            print("String representation of the Array of String needs to be supplied")
            self = Currency()
            return
        }

        guard let data = string.data(using: .utf8) else {
            print("String representation of the Array of String needs to be supplied")
            self = Currency()
            return
        }

        do {
            self = try Json().decoder.decode(Currency.self, from: data)
        } catch {
            print("String did not contain valid data to construct Currencies")
            self = Currency()
        }
    }
}

// MARK: - URLSession response handlers
// TODO: To be implemented once Currencies web service is available
extension URLSession {

    /// Obtain the List of available Currencies
    ///
    /// - Parameters:
    ///   - url: Currency List API
    ///   - completionHandler: API call completion 
    /// - Returns: Data Task
    func currencyTask(with url: URL, completionHandler: @escaping (Currency?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}
